package Models

import (
	"modfile/Config"

	_ "github.com/go-sql-driver/mysql"
)

func CreateOrder(order *Order) (err error) {
	if err = Config.DB.Create(&order).Error; err != nil {
		return err
	}
	return nil
}

func CreateItem(item *Item) (err error) {
	if err = Config.DB.Create(&item).Error; err != nil {
		return err
	}
	return nil
}

func GetOrder() (out []Order, err error) {
	err = Config.DB.Find(&out).Error
	return
}

func GetItem() (out []Item, err error) {
	err = Config.DB.Find(&out).Error
	return
}

func GetItemByID(id int) (out []Item, err error) {
	err = Config.DB.Table("items").Where("order_id = ?", id).Scan(&out).Error
	return
}

func UpdateOrder(order *Order, id string) (err error) {
	err = Config.DB.Table("orders").Where("order_id=?", id).Update(&order).Error
	return err
}

func DeleteOrder(id int) (err error) {
	err = Config.DB.Table("orders").Where("order_id = ?", id).Delete(nil).Error
	return nil
}

func DeleteItem(id int) (err error) {
	err = Config.DB.Table("items").Where("order_id = ?", id).Delete(nil).Error
	return nil
}
