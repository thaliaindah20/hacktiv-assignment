package Models

type Order struct {
	OrderID      int    `gorm:"primary_key;auto_increment" json:"order_id"`
	OrderedAt    string `gorm:"column:ordered_at" json:"ordered_at"`
	CustomerName string `gorm:"column:customer_name" json:"customer_name"`
	Items        []Item `json:"items"`
}

type Item struct {
	ItemID      int    `gorm:"primary_key;auto_increment" json:"item_id,omitempty"`
	ItemCode    string `gorm:"column:item_code" json:"item_code"`
	Description string `gorm:"column:description" json:"description"`
	Quantity    int    `gorm:"column:quantity" json:"quantity"`
	OrderID     int    `gorm:"column:order_id" json:"order_id,omitempty"`
}

func (Order) TableName() string {
	return "orders"
}

func (Item) TableName() string {
	return "items"
}
