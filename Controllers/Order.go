package Controllers

import (
	"fmt"
	"modfile/Models"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

func CreateOrder(c *gin.Context) {
	var newOrder Models.Order
	c.BindJSON(&newOrder)
	err := Models.CreateOrder(&newOrder)
	if err != nil {
		fmt.Println(err.Error())
		c.AbortWithStatus(http.StatusNotFound)
	}

	id := newOrder.OrderID
	for i, v := range newOrder.Items {
		newOrder.Items[i].OrderID = id
		err := Models.CreateItem(&newOrder.Items[i])
		if err != nil {
			c.AbortWithStatus(http.StatusNotFound)
		}
		fmt.Println(v)
	}
	c.JSON(http.StatusOK, newOrder)

}

func UpdateOrder(c *gin.Context) {
	url := c.Request.URL.Path
	orderID := url[8:]
	id, _ := strconv.Atoi(orderID)

	var updatedOrder Models.Order
	c.BindJSON(&updatedOrder)
	fmt.Println(updatedOrder)

	temp := Models.Order{
		OrderID:      id,
		OrderedAt:    updatedOrder.OrderedAt,
		CustomerName: updatedOrder.CustomerName,
	}

	err := Models.UpdateOrder(&temp, orderID)
	if err != nil {
		c.AbortWithStatus(http.StatusNotFound)
	}

	err = Models.DeleteItem(id)
	if err != nil {
		c.AbortWithStatus(http.StatusNotFound)
	}
	for i, v := range updatedOrder.Items {
		updatedOrder.Items[i].OrderID = id
		err := Models.CreateItem(&updatedOrder.Items[i])
		if err != nil {
			c.AbortWithStatus(http.StatusNotFound)
		}
		fmt.Println(v)
	}
	c.JSON(http.StatusOK, orderID+" is updated")
}

func GetOrder(c *gin.Context) {
	temp, err := Models.GetOrder()
	for i, v := range temp {
		item, err := Models.GetItemByID(v.OrderID)
		if err != nil {
			c.AbortWithStatus(http.StatusNotFound)
		}
		temp[i].Items = item
	}

	if err != nil {
		c.AbortWithStatus(http.StatusNotFound)
	} else {
		c.JSON(http.StatusOK, temp)
	}
}

func DeleteOrder(c *gin.Context) {
	url := c.Request.URL.Path
	orderID := url[8:]
	id, _ := strconv.Atoi(orderID)
	err := Models.DeleteItem(id)
	if err != nil {
		c.AbortWithStatus(http.StatusNotFound)
	}

	err = Models.DeleteOrder(id)
	if err != nil {
		c.AbortWithStatus(http.StatusNotFound)
	}

	c.JSON(http.StatusOK, gin.H{"order_id" + orderID: "is deleted"})
}
