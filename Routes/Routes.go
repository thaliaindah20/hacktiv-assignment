package Routes

import (
	"modfile/Controllers"

	"github.com/gin-gonic/gin"
)

func SetupRouter() *gin.Engine {
	router := gin.Default()

	router.POST("/orders", Controllers.CreateOrder)
	router.GET("/orders", Controllers.GetOrder)
	router.PUT("/orders/:orderID", Controllers.UpdateOrder)
	router.DELETE("/orders/:orderID", Controllers.DeleteOrder)
	return router
}
